# Lab 9 - Aldiyar Amirov 

## Check #1
### Website used: heroeswm.ru [FORGOT PASSWORD CHECK]
| Step                                                | Result                                                                                     |
|-----------------------------------------------------|--------------------------------------------------------------------------------------------|
| Open www.heroeswm.ru/login.php page                 | [SUCCESS] website is opened                                                                |
| Push "Забыли пароль" button                         | [SUCCESS] navigates to [restore page](https://www.heroeswm.ru/restorepsw.php)              |
| Create an account                                   | [SUCCESS] account is created                                                               |
| Try restore with created account credentials        | [SUCCESS] Got "Пароль выслан на Ваш e-mail!" message                                       |
| Enter non-existing credentials (username and email) | [FAILURE] Got "Неверный email: <non-existing email>" message (different from previous one) |
    
**RESULT** - *Failure*, different messages for existing and non-existing account restoring

## Check #2
### Website used: heroeswm.ru [AUTHENTICATION CHECK]
| Step                                                | Result                                                |
|-----------------------------------------------------|-------------------------------------------------------|
| Open www.heroeswm.ru/ page                          | [SUCCESS] website is opened                           |
| Create an account (Example username: Pegemon)       | [SUCCESS] account is created                          |
| Try to login with all uppercase letters in username | [SUCCESS] successful login, navigated to profile page |
| Try to login with all lowercase letters in username | [SUCCESS] successful login, navigated to profile page |

**RESULT** - *Successful*, authentication is case-insensetive for usernames
    
## Check #3
### Website used: heroeswm.ru [FORGOT PASSWORD] - Check if you can spam with restore password messages
| Step                                          | Result                                                                                                                                            |
|-----------------------------------------------|---------------------------------------------------------------------------------------------------------------------------------------------------|
| Open www.heroeswm.ru/login.php page           | [SUCCESS] website is opened                                                                                                                       |
| Create an account (Example username: Pegemon) | [SUCCESS] account is created                                                                                                                      |
| Try to restore account                        | [SUCCESS] successful restoring account message sent                                                                                               |
| Try to restore the same account again         | [SUCCESS] Got "...Повторный запрос восстановления возможен через 5 секунд." message (which says that we cannot do next request in 5 seconds only) |
    
**RESULT** - *Successful*, you there is some kind of request rate control
    
## Check #4
### Website used: heroeswm.ru [FORGOT PASSWORD] - Check if you get password in restore password message
| Step                                          | Result                                                          |
|-----------------------------------------------|-----------------------------------------------------------------|
| Open www.heroeswm.ru/ page                    | [SUCCESS] website is opened                                     |
| Create an account (Example username: Pegemon) | [SUCCESS] account is created                                    |
| Try to restore account                        | [FAILED] got temporary password in email, which is bad practice |
    
**RESULT** - *Failure*, got temporary password in restore password message.
